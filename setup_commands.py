from distutils.cmd import Command
from typing import Type, Dict
from distutils.command.build import build

import subprocess


def collect_commands() -> Dict[str, Type[Command]]:
    return {
        'build': BuildWithStaticCommand,
    }


class BuildWithStaticCommand(build):
    def run(self):
        self._build_static()
        super().run()
    
    def _build_static(self):
        yarn_dir_path = 'scatl/static/scatl/'
        self._install_static_dependencies(yarn_dir_path)
        self._build_static_with_webpack(yarn_dir_path)
        
    def _install_static_dependencies(self, yarn_dir_path: str):
        subprocess.run(
            ['yarn', 'install', '--pure-lockfile', '--cwd={}'.format(yarn_dir_path)],
            check=True,
        )

    def _build_static_with_webpack(self, yarn_dir_path: str):
        subprocess.run(
            ['yarn', 'run', '--cwd={}'.format(yarn_dir_path), 'build'],
            check=True,
        )
