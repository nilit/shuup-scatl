DEFAULT_TIMEOUT_INTERVAL = 9999999999999
Error.stackTraceLimit = Infinity


let webpackConfigProd = require('./webpack.conf')
let webpackConfigKarma = disableCommonsChunkPlugin(webpackConfigProd)


module.exports = function(config) {
    config.set({
        basePath: '.',
        
        frameworks: ['jasmine'],
        
        files: [
            'node_modules/faker/build/build/faker.js',
            'karma.shim.js',
        ],

        webpack: webpackConfigKarma,
        
        preprocessors: {
            'karma.shim.js': ['webpack', 'sourcemap']
        },
        
        webpackMiddleware: {stats: 'errors-only'},
        
        webpackServer: {noInfo: true},
        
        plugins: [
            require("karma-webpack"),
            require("karma-jasmine"),
            require("karma-chrome-launcher"),
            require("karma-firefox-launcher"),
            require("karma-sourcemap-loader"),
        ],

        exclude: [
            'e2e/**/*',
            'e2e/**/*',
            // or `@angular/compiler-cli` will break the tests
            'node_modules/**/*spec.js',
        ],

        proxies: {
            // required for component assets fetched by angular compiler
            '/app/': '/base/app/'
        },

        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: ['Firefox'],
        singleRun: true,
   })
}


/**
 * Because karma-webpack is going to fix CommonChunk support only in 3.0,
 * for details see webpack-contrib/karma-webpack#24.
 */
function disableCommonsChunkPlugin(webpackConfig) {
    let webpack = require("webpack")
    let pluginsWithoutCommonsChunk = webpackConfig.plugins.filter(plugin => {
        let isNotCommonsChunkPlugin = !(plugin instanceof webpack.optimize.CommonsChunkPlugin)
        return isNotCommonsChunkPlugin
    })
    webpackConfig.plugins = pluginsWithoutCommonsChunk
    return webpackConfig
}
