"use strict"
const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')


module.exports = {
    entry: {
        'polyfills': './app/polyfills.ts',
        'vendor': './app/vendor.ts',
        'app': './app/main.ts',
    },

    resolve: {
        extensions: ['.ts', '.js'],
        modules: [
            path.resolve('.'),
            path.resolve('./node_modules'),
        ]
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: [
                    {
                        loader: 'awesome-typescript-loader',
                        options: {configFileName: './tsconfig.json'}
                    },
                    'angular2-template-loader',
                ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[ext]',
            },
            {
                test: /\.css$/,
                exclude: path.resolve('./app'),
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader'
                })
            },
            {
                test: /\.css$/,
                include: path.resolve('./app'),
                loader: 'raw-loader',
            }
        ]
    },

    plugins: [
        // a workaround for angular/angular#11580
        new webpack.ContextReplacementPlugin(
            // the `[\\\/]` pieces account for path separators in *nix and Windows
            /angular[\\\/]core[\\\/]@angular/,
            './app',
            {}
        ),
        
        new webpack.NoEmitOnErrorsPlugin(),

        new ExtractTextPlugin('[name].css'),

        // a workaround for ng2
        new webpack.LoaderOptionsPlugin({htmlLoader: {minimize: false}}),
        
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),
    ],
    
    output: {
        path: path.resolve('./dist'),
        publicPath: '/static/scatl/dist/',
        filename: '[name].js',
    },
}
