export interface Window {
    DJANGO: DjangoProvidedGlobals
}


export interface DjangoProvidedGlobals {
    user: {
        isAuthenticated: boolean
        username?: string
    }
    priceRange: {
        min: number
        max: number
    }
    staticUrl: string
}
