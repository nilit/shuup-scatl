import {Component} from '@angular/core'


@Component({
    selector: 'scatl',
    template: `
        <router-outlet></router-outlet>
    `,
})
export class AppComponent { }
