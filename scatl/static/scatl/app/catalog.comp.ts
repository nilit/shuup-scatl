import {Component} from '@angular/core'
import {ViewEncapsulation} from '@angular/core'


@Component({
    selector: 'catalog',
    templateUrl: 'catalog.comp.html',
    styleUrls: ['catalog.comp.css'],
    encapsulation: ViewEncapsulation.None,
})
export class CatalogComponent { }
