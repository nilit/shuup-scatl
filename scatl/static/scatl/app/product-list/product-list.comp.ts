import {Component} from '@angular/core'
import {ProductListService} from 'app/shared/product-list/product-list.service'
import {QueryDataService} from 'app/shared/query-data/query-data.service'
import {asyncWithLoadingBar} from 'app/shared/decorators/async-with-loading-bar.decorator'
import {Loadable} from 'app/shared/decorators/async-with-loading-bar.decorator'
import {SlimLoadingBarService} from 'ng2-slim-loading-bar'


@Component({
    selector: 'product-list',
    templateUrl: 'product-list.comp.html',
    styleUrls: ['product-list.comp.css'],
})
export class ProductListComponent implements Loadable {
    // noinspection JSUnusedGlobalSymbols
    constructor(
        public loadingBarService: SlimLoadingBarService,
        protected queryDataService: QueryDataService,
        protected productListService: ProductListService
    ) { }
    
    @asyncWithLoadingBar
    async handlePaginationUpdate() {
        await this.queryDataService.updateQueryData()
        await this.productListService.updateList()
    }
}
