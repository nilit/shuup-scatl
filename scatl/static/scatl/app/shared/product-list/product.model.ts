export interface Product {
    name: string
    price: number
    url: string
    imageUrl: string
}
