import 'core-js/es6'
import 'core-js/es7/reflect'
import 'zone.js/dist/zone'
import 'zone.js/dist/long-stack-trace-zone'
import 'zone.js/dist/proxy'
import 'zone.js/dist/sync-test'
import 'zone.js/dist/jasmine-patch'


declare var __karma__: any


initializeKarma()


function initializeKarma() {
    stopKarmaDefaultInit()

    try {
        initAngularTestEnvironment()
        loadAngularTestCases()
        __karma__.start()
    } catch (error) {
        console.log(error)
        __karma__.error(error)
    }
}


function stopKarmaDefaultInit() {
    __karma__.loaded = () => {}
}


function initAngularTestEnvironment() {
    let coreTestingModule = require('@angular/core/testing')
    let platformBrowserModule = require('@angular/platform-browser-dynamic/testing')

    coreTestingModule.TestBed.initTestEnvironment(
        platformBrowserModule.BrowserDynamicTestingModule,
        platformBrowserModule.platformBrowserDynamicTesting(),
    )
}


function loadAngularTestCases() {
    let appContext = (require as any).context('./app', true, /\.spec\.ts/)
    appContext.keys().forEach(appContext)
}
