import {browser} from 'protractor'
import {expectProducts} from './utils/matchers'
import {setPriceInput} from './utils/utils'
import {inputs} from './utils/django'
import {products} from './utils/django'
import {BASE_URL} from './utils/django'
import {$} from 'protractor'


describe('control form price range', () => {
    beforeAll(async done => {
        await browser.get(BASE_URL)
        done()
    })

    it('filters by max price', async done => {
        await setPriceInput(inputs.priceMin, 0)
        await setPriceInput(inputs.priceMax, 1000)
        await expectProducts(products.ava)
        done()
    })

    it('filters by min price', async done => {
        await setPriceInput(inputs.priceMin, 1200)
        await setPriceInput(inputs.priceMax, 10000)
        await expectProducts(products.kas)
        done()
    })

    it('filters by min and max price', async done => {
        await setPriceInput(inputs.priceMin, 1000)
        await setPriceInput(inputs.priceMax, 1300)
        await expectProducts(products.nod)
        done()
    })
    
    it('inits the from values from url', async done => {
        let priceMinNew = 950
        let priceMaxNew = 1000
        let url = `${BASE_URL};filter.price=${priceMinNew}~${priceMaxNew};`
        await browser.get(url)
        
        let sliderBar = $('.noUi-connect')
        
        let sliderMaxShift: string = await sliderBar.getCssValue('right')
        let sliderMaxShiftDefault = '0%'
        expect(sliderMaxShift).not.toBe(sliderMaxShiftDefault)
        
        let sliderMinShift: string = await sliderBar.getCssValue('left')
        let sliderMinShiftDefault = '0%'
        expect(sliderMinShift).not.toBe(sliderMinShiftDefault)
        
        let priceMinInputValue = await $('[name=price-min]').getAttribute('value')
        expect(priceMinInputValue).toBe(String(priceMinNew))
        
        let priceMaxInputValue = await $('[name=price-max]').getAttribute('value')
        expect(priceMaxInputValue).toBe(String(priceMaxNew))
        
        done()
    })
})
